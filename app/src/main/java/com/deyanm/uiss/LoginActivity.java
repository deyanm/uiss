package com.deyanm.uiss;

import static com.deyanm.uiss.retrofit.APIClient.BASE_URL;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.deyanm.uiss.retrofit.APIClient;
import com.deyanm.uiss.retrofit.APIService;
import com.deyanm.uiss.utils.ActivityUtils;
import com.deyanm.uiss.utils.EncryptionServices;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();
    APIService apiService;
    ProgressDialog progressDialog;
    private CheckBox rememberMe;
    private EditText egn, fak;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private RelativeLayout loginLayout;
    private Fragment fragment;
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        createKeysInAndroidKeystore();

        mPreferences = this.getSharedPreferences("UISS-APP", Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();

        apiService = APIClient.getClient(this, false).create(APIService.class);

        egn = findViewById(R.id.inputEgn);
        fak = findViewById(R.id.inputFak);
        rememberMe = findViewById(R.id.remember);
        loginLayout = findViewById(R.id.loginLayout);

//        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
//
//        View popupView = layoutInflater.inflate(R.layout.popup_layout, null);
//
//        popUp = new PopupWindow(popupView, 300, 300, true);
//
//        popUp.setTouchable(true);
//        popUp.setFocusable(true);
//
//        popupView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
//        popUp.showAtLocation(popupView, Gravity.CENTER, 0, 0);
//        popUp.update(0, 0, popUp.getWidth(), popUp.getHeight());

        setupWebview();

        progressDialog = showLoadingDialog(this);

        if (mPreferences.getBoolean("remember", false) && mPreferences.contains("fak")) {
            String fakDecrypted = new EncryptionServices(getApplicationContext()).decrypt(mPreferences.getString("fak", "fak"));
            String egnDecrypted = new EncryptionServices(getApplicationContext()).decrypt(mPreferences.getString("egn", "fak"));
            if (fakDecrypted != null && egnDecrypted != null) {
                rememberMe.setChecked(true);
                fak.setText(fakDecrypted);
                egn.setText(egnDecrypted);
            } else {
                mEditor.remove("fak");
                mEditor.remove("egn");
                mEditor.putBoolean("remember", false);
                mEditor.apply();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mWebView.getVisibility() == View.GONE) {
            mWebView.setVisibility(View.VISIBLE);
        }
    }

    private void setupWebview() {
        mWebView = findViewById(R.id.webView);
        CookieSyncManager.createInstance(this);
        CookieSyncManager.getInstance().startSync();
        CookieManager cookieManager = CookieManager.getInstance();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            cookieManager.setAcceptThirdPartyCookies(mWebView, true);
        } else {
            cookieManager.setAcceptCookie(true);
        }
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setJavaScriptEnabled(true);
//        mWebView.setVerticalScrollBarEnabled(false);
//        mWebView.setOnTouchListener((v, event) -> (event.getAction() == MotionEvent.ACTION_MOVE));
//        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.addJavascriptInterface(new WebViewJavascriptInterface(), "HTMLOUT");
        mWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mWebView.getSettings().setUserAgentString("myUISS App (Android SDK Version: " + Build.VERSION.SDK_INT + ", Device: " + Build.MANUFACTURER + ", Model: " + Build.MODEL + ")");
        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.contains(BASE_URL)) {
                    return super.shouldOverrideUrlLoading(view, url);
                }
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mWebView.evaluateJavascript("javascript:document.getElementsByClassName('cookieinfo-close')[0].click();", value -> {
                });
                mWebView.evaluateJavascript("javascript:(function() { " +
                        "document.getElementsByTagName('table')[0].rows[0].style.display='none';" +
                        "document.getElementsByTagName('table')[0].rows[1].cells[0].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('table')[0].rows[0].cells[0].style.display='none';" +
                        "document.getElementsByTagName('table')[0].rows[2].style.display='none';" +
                        "document.getElementsByTagName('table')[0].rows[3].style.display='none';" +
                        "document.getElementsByTagName('table')[0].rows[1].cells[0].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('table')[0].rows[0].cells[1].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('form')[0].getElementsByTagName('table')[0].rows[0].style.display='none';" +
                        "document.getElementsByTagName('table')[0].rows[1].cells[0].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('table')[0].rows[0].cells[1].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('form')[0].getElementsByTagName('table')[0].rows[1].style.display='none';" +
                        "document.getElementsByTagName('table')[0].rows[1].cells[0].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('table')[0].rows[0].cells[1].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('form')[0].getElementsByTagName('table')[0].rows[3].style.display='none';" +
                        "document.getElementsByClassName('login_table')[0].classList.remove('login_table');" +
                        "})()", value -> {
                });
                mWebView.evaluateJavascript("javascript:HTMLOUT.resize(document.body.scrollHeight)", value -> {
                });
                Runnable helloRunnable = () -> mWebView.post(() -> {
                    if (mWebView.getContentHeight() > 400) {
                        ViewGroup.LayoutParams vc = mWebView.getLayoutParams();
                        vc.height = ActivityUtils.dpToPx(300);
                        mWebView.setLayoutParams(vc);
                    } else {
                        ViewGroup.LayoutParams vc = mWebView.getLayoutParams();
                        vc.height = ActivityUtils.dpToPx(100);
                        mWebView.setLayoutParams(vc);
                    }
                });
                ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
                executor.scheduleAtFixedRate(helloRunnable, 0, 1, TimeUnit.SECONDS);

                new Timer().schedule(new TimerTask() {
                    @Override
                    public void run() {
                        hideLoading();
                    }
                }, 3000);
                mWebView.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }
        });
        mWebView.loadUrl("https://student.tu-sofia.bg/");
    }

    public void login(View view) {
        if (fak.getText().toString().isEmpty()) {
            fak.setError("Моля въведете факултетен номер!");
            return;
        }
        if (egn.getText().toString().isEmpty()) {
            egn.setError("Моля въведете ЕГН!");
            return;
        }
        if (rememberMe.isChecked()) {
            mEditor.putString("fak", new EncryptionServices(getApplicationContext()).encrypt(fak.getText().toString()));
            mEditor.putString("egn", new EncryptionServices(getApplicationContext()).encrypt(egn.getText().toString()));
            mEditor.putBoolean("remember", true);
            mEditor.apply();
        } else {
            mEditor.remove("fak");
            mEditor.remove("egn");
            mEditor.putBoolean("remember", false);
            mEditor.apply();
        }
        mWebView.setVisibility(View.GONE);
        mWebView.evaluateJavascript("javascript:document.getElementById('egn').value='" + egn.getText().toString() + "';", s -> {
        });
        mWebView.evaluateJavascript("javascript:document.getElementById('fn').value='" + fak.getText().toString() + "';", s -> {

        });
        mWebView.evaluateJavascript("javascript:document.getElementById('login').click();", s -> {
            if (s == null) {
                mWebView.loadUrl("https://student.tu-sofia.bg/");
            }
        });

        progressDialog = showLoadingDialog(this);
//        final String js = "javascript:document.getElementById('egn').value='" + egn.getText().toString() + "';" +
//                "javascript:document.getElementById('fn').value='" + fak.getText().toString() + "';" +
//                "document.getElementById('login').click()";
//        apiService.login(egn.getText().toString(), fak.getText().toString()).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                loginToServer();
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                hideLoading();
//                String cause = "";
//                if (t instanceof IOException) {
//                    cause = "Проблем със свързването, проверете дали имате активна интернет връзка!";
//                } else {
//                    cause = String.valueOf(t.getMessage());
//                }
//                ActivityUtils.showSnackBar(findViewById(android.R.id.content), cause);
//                call.cancel();
//            }
//        });
    }

    private void createKeysInAndroidKeystore() {
        EncryptionServices encryptionService = new EncryptionServices(this.getApplicationContext());

        if (!encryptionService.doesAndroidMasterKeyExist()) {
            encryptionService.createMasterKey("3f394a452787428c14492fefece18b33");
        }

    }

    public static ProgressDialog showLoadingDialog(Context context) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        if (progressDialog.getWindow() != null) {
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
        progressDialog.setContentView(R.layout.progress_dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

    public void hideLoading() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.cancel();
        }
    }

    public void displayHtml(String html) {
        hideLoading();
        Document document = Jsoup.parse(html);
        if (!document.select("span.red").text().contains("не беше намерен")) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class).putExtra("html", html));
            finish();
        } else {
            ActivityUtils.showSnackBar(findViewById(android.R.id.content), "Студентът не беше намерен!");
        }
    }

    // actual login
    public void loginToServer() {
        apiService.login(egn.getText().toString(), fak.getText().toString()).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    String html = response.body().string();
                    Log.d(TAG, html);
                    hideLoading();
                    Document document = Jsoup.parse(html);
                    if (!document.select("span.red").text().contains("не беше намерен")) {
                        startActivity(new Intent(LoginActivity.this, MainActivity.class).putExtra("html", html));
                        finish();
                    } else {
                        ActivityUtils.showSnackBar(findViewById(android.R.id.content), "Студентът не беше намерен!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, Throwable t) {
                hideLoading();
                String cause = "";
                if (t instanceof IOException) {
                    cause = "Проблем със свързването, проверете дали имате активна интернет връзка!";
                } else {
                    cause = String.valueOf(t.getMessage());
                }
                ActivityUtils.showSnackBar(findViewById(android.R.id.content), cause);
                call.cancel();
            }
        });
    }

    public void loginEtus(View view) {
        if (fak.getText().toString().isEmpty()) {
            fak.setError("Моля въведете факултетен номер!");
            return;
        }
        if (egn.getText().toString().isEmpty()) {
            egn.setError("Моля въведете ЕГН!");
            return;
        }
        if (rememberMe.isChecked()) {
            mEditor.putString("fak", new EncryptionServices(getApplicationContext()).encrypt(fak.getText().toString()));
            mEditor.putString("egn", new EncryptionServices(getApplicationContext()).encrypt(egn.getText().toString()));
            mEditor.putBoolean("remember", true);
            mEditor.apply();
        } else {
            mEditor.remove("fak");
            mEditor.remove("egn");
            mEditor.putBoolean("remember", false);
            mEditor.apply();
        }

//        callPopup();
//        progressDialog = showLoadingDialog(this);
//        apiService.loginETUS(egn.getText().toString(), fak.getText().toString()).enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
//                hideLoading();
//                loginLayout.setVisibility(View.GONE);
//                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
//                fragmentTransaction.replace(R.id.activity_login, fragment);
//                fragmentTransaction.commit();
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
//                hideLoading();
//                String cause = "";
//                if (t instanceof IOException) {
//                    cause = "Проблем със свързването, проверете дали имате активна интернет връзка!";
//                } else {
//                    cause = String.valueOf(t.getMessage());
//                }
//                ActivityUtils.showSnackBar(LoginActivity.this.findViewById(android.R.id.content), cause);
//                call.cancel();
//            }
//        });
    }

    @Override
    public void onBackPressed() {
        if (loginLayout.getVisibility() == View.GONE) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            if (fragment != null) {
                transaction.remove(fragment);
                transaction.commit();
                transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
                fragment = null;
            }
            loginLayout.setVisibility(View.VISIBLE);
        }
    }


    class WebViewJavascriptInterface {

        @JavascriptInterface
        public void resize(final float height) {
            float webViewHeight = (height * getResources().getDisplayMetrics().density);
            Log.d(TAG, "HEIGHT: " + webViewHeight);
            //webViewHeight is the actual height of the WebView in pixels as per device screen density
        }

        @JavascriptInterface
        public void processHTML(String html) {
            runOnUiThread(() -> {
                if (html.contains("Специалност")) {
                    displayHtml(html);
                }
                hideLoading();
                Document document = Jsoup.parse(html);
                if (document.select("span.red").text().contains("не беше намерен")) {
                    ActivityUtils.showSnackBar(findViewById(android.R.id.content), "Студентът не беше намерен!");
                    mWebView.loadUrl("https://student.tu-sofia.bg/");
                    mWebView.setVisibility(View.VISIBLE);
                }
            });
        }
    }

//    private void callPopup() {
//
//        LayoutInflater layoutInflater = (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
//
//        View popupView = layoutInflater.inflate(R.layout.popup_layout, null);
//
//        popUp = new PopupWindow(popupView, 300, 300, true);
//
//        popUp.setTouchable(true);
//        popUp.setFocusable(true);
//
//        popupView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
//        popUp.showAtLocation(popupView, Gravity.CENTER, 0, 0);
//        popUp.update(0, 0, popUp.getWidth(), popUp.getHeight());
//
//        WebView webView = popupView.findViewById(R.id.webView);
//        CookieSyncManager.createInstance(this);
//        CookieSyncManager.getInstance().startSync();
//        CookieManager cookieManager = CookieManager.getInstance();
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            cookieManager.setAcceptThirdPartyCookies(webView, true);
//        } else {
//            cookieManager.setAcceptCookie(true);
//        }
//        webView.getSettings().setDomStorageEnabled(true);
//        webView.getSettings().setJavaScriptEnabled(true);
//        webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
//        webView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
//        webView.setWebViewClient(new WebViewClient() {
//
//            @Override
//            public boolean shouldOverrideUrlLoading(WebView view, String url) {
//                return super.shouldOverrideUrlLoading(view, url);
//
//            }
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                super.onPageFinished(view, url);
//                webView.evaluateJavascript("javascript:(function() { " +
//                        "document.getElementsByTagName('table')[0].rows[0].style.display='none';" +
//                        "document.getElementsByTagName('table')[0].rows[1].cells[0].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('table')[0].rows[0].cells[0].style.display='none';" +
//                        "document.getElementsByTagName('table')[0].rows[2].style.display='none';" +
//                        "document.getElementsByTagName('table')[0].rows[3].style.display='none';" +
//                        "document.getElementsByTagName('table')[0].rows[1].cells[0].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('table')[0].rows[0].cells[1].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('form')[0].getElementsByTagName('table')[0].rows[0].style.display='none';" +
//                        "document.getElementsByTagName('table')[0].rows[1].cells[0].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('table')[0].rows[0].cells[1].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('form')[0].getElementsByTagName('table')[0].rows[1].style.display='none';" +
//                        "document.getElementsByTagName('table')[0].rows[1].cells[0].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('table')[0].rows[0].cells[1].getElementsByTagName('table')[0].rows[0].cells[0].getElementsByTagName('form')[0].getElementsByTagName('table')[0].rows[3].style.display='none';" +
//                        "})()", value -> {
//                    Log.d(TAG, value);
//                });
//                webView.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
//                if (url.contains("info.php")) {
//                    String cookies = CookieManager.getInstance().getCookie(view.getUrl());
//                    String cookie = cookies.split(";")[0];
//                    Log.d(TAG, cookie);
//                }
//            }
//        });
//        webView.loadUrl("https://student.tu-sofia.bg/");
//    }
}
