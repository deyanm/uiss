package com.deyanm.uiss;

import static com.deyanm.uiss.retrofit.APIClient.BASE_URL;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.deyanm.uiss.fragments.HealthFragment;
import com.deyanm.uiss.fragments.HistoryFragment;
import com.deyanm.uiss.fragments.InfoFragment;
import com.deyanm.uiss.fragments.MarksFragment;
import com.deyanm.uiss.fragments.SettingsFragment;
import com.deyanm.uiss.fragments.SignFragment;
import com.deyanm.uiss.retrofit.APIClient;
import com.deyanm.uiss.retrofit.APIService;
import com.google.android.material.navigation.NavigationView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private APIService apiService;

    private View header;
    private String htmlData = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        apiService = APIClient.getClient(this, false).create(APIService.class);

        header = navigationView.getHeaderView(0);

        htmlData = getIntent().getStringExtra("html");
        setupNavigationHeader(htmlData);
        onNavigationItemSelected(navigationView.getMenu().getItem(0));
        navigationView.setCheckedItem(R.id.nav_info);

        apiService.getStudentPhoto(CookieManager.getInstance().getCookie(BASE_URL)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.body() != null) {
                    Bitmap bmp = BitmapFactory.decodeStream(response.body().byteStream());
                    ((ImageView) header.findViewById(R.id.accountPhoto)).setImageBitmap(bmp);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, Throwable t) { ;
                Log.d(TAG, t.getLocalizedMessage());
                call.cancel();
            }
        });

//        apiService.getStudentInfo().enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                try {
//                    htmlData = response.body().string();
//                    mapData(htmlData);
//                    onNavigationItemSelected(navigationView.getMenu().getItem(0));
//                    navigationView.setCheckedItem(R.id.nav_info);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                String cause = "";
//                if (t instanceof IOException) {
//                    cause = "Проблем със свързването, проверете дали имате активна интернет връзка!";
//                } else {
//                    cause = String.valueOf(t.getMessage());
//                }
//                ActivityUtils.showSnackBar(findViewById(android.R.id.content), cause);
//                call.cancel();
//            }
//        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.rate) {
            createAppRatingDialog(getString(R.string.rate_app_title), getString(R.string.rate_app_message)).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private AlertDialog createAppRatingDialog(String rateAppTitle, String rateAppMessage) {
        return new AlertDialog.Builder(this).setPositiveButton(getString(R.string.dialog_app_rate), (paramAnonymousDialogInterface, paramAnonymousInt) -> {
            openAppInPlayStore(MainActivity.this);
        }).setNegativeButton(getString(R.string.dialog_your_feedback), (paramAnonymousDialogInterface, paramAnonymousInt) -> {
            openFeedback(MainActivity.this);
        }).setNeutralButton(getString(R.string.dialog_ask_later), (paramAnonymousDialogInterface, paramAnonymousInt) -> {
            paramAnonymousDialogInterface.dismiss();
        }).setMessage(rateAppMessage).setTitle(rateAppTitle).create();
    }

    public void openAppInPlayStore(Context paramContext) {
        paramContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
    }

    public void openFeedback(Context paramContext) {
        Intent localIntent = new Intent(Intent.ACTION_SEND);
        localIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"deiandeni13@gmail.com"});
        localIntent.putExtra(Intent.EXTRA_CC, "deyanm1999@gmail.com");
        String str = null;
        try {
            str = paramContext.getPackageManager().getPackageInfo(paramContext.getPackageName(), 0).versionName;
            localIntent.putExtra(Intent.EXTRA_SUBJECT, "Мнение за приложението");
            localIntent.putExtra(Intent.EXTRA_TEXT, "\n\n----------------------------------\n Device OS: Android \n Device OS version: " +
                    Build.VERSION.RELEASE + "\n App Version: " + str + "\n Device Brand: " + Build.BRAND +
                    "\n Device Model: " + Build.MODEL + "\n Device Manufacturer: " + Build.MANUFACTURER);
            localIntent.setType("message/rfc822");
            paramContext.startActivity(Intent.createChooser(localIntent, "Изберете e-mail клиент:"));
        } catch (Exception e) {
            Log.d("OpenFeedback", e.getMessage());
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Fragment fragment = null;
        if (id == R.id.nav_info) {
            fragment = InfoFragment.newInstance(htmlData);
            displaySelectedFragment(fragment);
        } else if (id == R.id.nav_grades) {
            fragment = new MarksFragment();
            displaySelectedFragment(fragment);
        } else if (id == R.id.nav_sign) {
            fragment = new SignFragment();
            displaySelectedFragment(fragment);
//        else if (id == R.id.nav_sign_etus) {
//            Bundle b = null;
//            if (getIntent().hasExtra("personData")) {
//                b = getIntent().getBundleExtra("personData");
//            }
//            fragment = SignETUSFragment.newInstance(b);
//            displaySelectedFragment(fragment);
        } else if (id == R.id.nav_hosp) {
            fragment = new HealthFragment();
            displaySelectedFragment(fragment);
        } else if (id == R.id.nav_history) {
            fragment = new HistoryFragment();
            displaySelectedFragment(fragment);
        } else if (id == R.id.nav_settings) {
            fragment = new SettingsFragment();
            displaySelectedFragment(fragment);
        } else if (id == R.id.nav_exit) {
            logout();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void logout() {
        apiService.logout().enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, response.body().toString());
                getSharedPreferences("UISS-APP", Context.MODE_PRIVATE).edit().remove("PREF_COOKIES").apply();
                startActivity(new Intent(MainActivity.this, LoginActivity.class));
                finish();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void setupNavigationHeader(String html) {
        Document document = Jsoup.parse(html);
        ((TextView) header.findViewById(R.id.acccountName)).setText(document.select("td.dashed_links").get(1).select("strong").text());

        TextView textView = header.findViewById(R.id.accountEmail);
        textView.setClickable(true);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        Elements linkEl = document.select("td[align=left]").select("a[href^=\"https\"]");
        String link = "<a href='" + linkEl.attr("href") + "'>" + linkEl.text() + "</a>";
        textView.setText(Html.fromHtml(link));
    }

    private void displaySelectedFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();
    }

}