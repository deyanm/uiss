package com.deyanm.uiss.data;

public class Mark {
    private String id;
    private String disc;
    private String form;
    private String mark;
    private String latest;

    public Mark(String id, String disc, String form, String mark, String latest) {
        this.id = id;
        this.disc = disc;
        this.form = form;
        this.mark = mark;
        this.latest = latest;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }

    public String getForm() {
        return form;
    }

    public void setForm(String form) {
        this.form = form;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getLatest() {
        return latest;
    }

    public void setLatest(String latest) {
        this.latest = latest;
    }
}