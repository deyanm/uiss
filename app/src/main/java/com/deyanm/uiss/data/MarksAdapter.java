package com.deyanm.uiss.data;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.deyanm.uiss.R;

import java.util.List;

public class MarksAdapter extends RecyclerView.Adapter<MarksAdapter.MarksViewHolder> {

    private List<Semester> semesters;
    private Context context;
    private RecyclerView recyclerView;

    public MarksAdapter(Context context, List<Semester> semesters) {
        this.context = context;
        this.semesters = semesters;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @NonNull
    @Override
    public MarksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_marks_vh, parent, false);
        return new MarksViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MarksViewHolder holder, int position) {

        Semester semester = semesters.get(position);

        if (semester != null) {
            if (semester.getTitle() != null) {
                holder.mTitle.setText(Html.fromHtml(semester.getTitle()));
            }
            if (semester.getList() != null && semester.getList().size() != 0) {
                int childSize = semester.getList().size();
                holder.childItems.removeAllViews();
                View tableHeader = View.inflate(context, R.layout.sem_header_marks, null);
                holder.childItems.addView(tableHeader);
                for (int i = 0; i < childSize; i++) {
                    View rootView = View.inflate(context, R.layout.item_marks, null);
                    LinearLayout currentChildLayout = rootView.findViewById(R.id.marksRoot);
                    List<Mark> marks = (List<Mark>) semester.getList();
                    ((TextView) currentChildLayout.findViewById(R.id.txtCount)).setText(marks.get(i).getId());
                    ((TextView) currentChildLayout.findViewById(R.id.txtDisc)).setText(marks.get(i).getDisc());
                    ((TextView) currentChildLayout.findViewById(R.id.txtForm)).setText(marks.get(i).getForm());
                    ((TextView) currentChildLayout.findViewById(R.id.txtGrade)).setText(marks.get(i).getMark());
                    ((TextView) currentChildLayout.findViewById(R.id.txtLat)).setText(marks.get(i).getLatest());
                    holder.childItems.addView(rootView);
                }
            } else {
                holder.imageView.setVisibility(View.GONE);
            }
        }

    }

    @Override
    public int getItemCount() {
        return semesters != null ? semesters.size() : 0;
    }

    public class MarksViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mTitle;
        private LinearLayout childItems;
        private ImageView imageView;

        public MarksViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTitle = itemView.findViewById(R.id.title);
            imageView = itemView.findViewById(R.id.icon);
            childItems = itemView.findViewById(R.id.childItems);
            childItems.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {
            if (childItems.getVisibility() == View.VISIBLE) {
                imageView.setImageResource(R.drawable.ic_keyboard_arrow_right_black_24dp);
                childItems.setVisibility(View.GONE);
            } else {
                if (childItems.getChildCount() > 0) {
                    for (int i = 0; i < recyclerView.getChildCount(); i++) {
                        MarksViewHolder holder1 = (MarksViewHolder) recyclerView.getChildViewHolder(recyclerView.getChildAt(i));
                        if (i != getLayoutPosition()) {
                            holder1.imageView.setImageResource(R.drawable.ic_keyboard_arrow_right_black_24dp);
                            holder1.childItems.setVisibility(View.GONE);
                        }
                    }
                }
                imageView.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                childItems.setVisibility(View.VISIBLE);
            }
        }
    }
}