package com.deyanm.uiss.data;

import java.util.List;

public class Semester {
    private String title;

    private List<?> list;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<?> getList() {
        return list;
    }

    public void setList(List<?> list) {
        this.list = list;
    }
}
