package com.deyanm.uiss.data;

import android.icu.lang.UProperty;

public class Sign {
    private String id;
    private String disc;
    private String lect;
    private String sem;
    private String lab;
    private String pract;
    private String proj;

    private Status isLectSign, isSemSign, isLabSign, isPractSign, isProjSign;

    public Sign() {

    }

    public Sign(String id, String disc, String lect, String sem, String lab, String pract, String proj) {
        this.id = id;
        this.disc = disc;
        this.lect = lect;
        this.sem = sem;
        this.lab = lab;
        this.pract = pract;
        this.proj = proj;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }

    public String getLect() {
        return lect;
    }

    public void setLect(String lect) {
        this.lect = lect;
    }

    public String getSem() {
        return sem;
    }

    public void setSem(String sem) {
        this.sem = sem;
    }

    public String getLab() {
        return lab;
    }

    public void setLab(String lab) {
        this.lab = lab;
    }

    public String getPract() {
        return pract;
    }

    public void setPract(String pract) {
        this.pract = pract;
    }

    public String getProj() {
        return proj;
    }

    public void setProj(String proj) {
        this.proj = proj;
    }

    public Status getIsLectSign() {
        return isLectSign;
    }

    public void setIsLectSign(Status isLectSign) {
        this.isLectSign = isLectSign;
    }

    public Status getIsSemSign() {
        return isSemSign;
    }

    public void setIsSemSign(Status isSemSign) {
        this.isSemSign = isSemSign;
    }

    public Status getIsLabSign() {
        return isLabSign;
    }

    public void setIsLabSign(Status isLabSign) {
        this.isLabSign = isLabSign;
    }

    public Status getIsPractSign() {
        return isPractSign;
    }

    public void setIsPractSign(Status isPractSign) {
        this.isPractSign = isPractSign;
    }

    public Status getIsProjSign() {
        return isProjSign;
    }

    public void setIsProjSign(Status isProjSign) {
        this.isProjSign = isProjSign;
    }
}