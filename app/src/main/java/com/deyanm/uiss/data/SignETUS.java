package com.deyanm.uiss.data;

public class SignETUS {

    private String disc;

    private String lecText, labText, semText, pracText;
    private Status isLecSign, isLabSign, isSemSign, isPracSign;

    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }

    public String getLecText() {
        return lecText;
    }

    public void setLecText(String lecText) {
        this.lecText = lecText;
    }

    public String getLabText() {
        return labText;
    }

    public void setLabText(String labText) {
        this.labText = labText;
    }

    public String getSemText() {
        return semText;
    }

    public void setSemText(String semText) {
        this.semText = semText;
    }

    public String getPracText() {
        return pracText;
    }

    public void setPracText(String pracText) {
        this.pracText = pracText;
    }

    public Status getIsLecSign() {
        return isLecSign;
    }

    public void setIsLecSign(Status isLecSign) {
        this.isLecSign = isLecSign;
    }

    public Status getIsLabSign() {
        return isLabSign;
    }

    public void setIsLabSign(Status isLabSign) {
        this.isLabSign = isLabSign;
    }

    public Status getIsSemSign() {
        return isSemSign;
    }

    public void setIsSemSign(Status isSemSign) {
        this.isSemSign = isSemSign;
    }

    public Status getIsPracSign() {
        return isPracSign;
    }

    public void setIsPracSign(Status isPracSign) {
        this.isPracSign = isPracSign;
    }
}
