package com.deyanm.uiss.data;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.deyanm.uiss.R;

import java.util.List;

public class SignEtAdapter extends RecyclerView.Adapter<SignEtAdapter.SignEtSemViewHolder> {

    private List<Semester> semesters;
    private Context mContext;
    private RecyclerView recyclerView;

    public SignEtAdapter(Context context, List<Semester> semesters) {
        mContext = context;
        this.semesters = semesters;
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }


    @NonNull
    @Override
    public SignEtSemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SignEtSemViewHolder(LayoutInflater.from(parent.getContext() ).inflate(R.layout.item_marks_vh, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull SignEtSemViewHolder holder, final int position) {
        Semester semester = semesters.get(position);

        if (semester != null) {
            if (semester.getTitle() != null) {
                holder.mTitle.setText(Html.fromHtml(semester.getTitle()));
            }
            if (semester.getList() != null && semester.getList().size() != 0) {
                int childSize = semester.getList().size();
                holder.childItems.removeAllViews();
                View tableHeader = View.inflate(mContext, R.layout.sem_header_marks, null);
                holder.childItems.addView(tableHeader);
                for (int i = 0; i < childSize; i++) {
                    View rootView = View.inflate(mContext, R.layout.item_sign_etus, null);
                    LinearLayout currentChildLayout = rootView.findViewById(R.id.marksRoot);
                    List<SignETUS> signETUSES = (List<SignETUS>) semester.getList();
                    ((TextView) currentChildLayout.findViewById(R.id.txtEtDisc)).setText(signETUSES.get(i).getDisc());
                    int finalI = i;
                    // lec
                    ImageView lecView = currentChildLayout.findViewById(R.id.txtEtLections);
                    switch (signETUSES.get(finalI).getIsLecSign()) {
                        case PASSED:
                            lecView.setImageResource(R.drawable.ic_check_24dp);
                            break;
                        case FAILED:
                            lecView.setImageResource(R.drawable.ic_close_24dp);
                            break;
                        case BLANK:
                            lecView.setImageResource(0);
                            break;
                    }
                    lecView.setOnClickListener(v -> {
                        if (signETUSES.get(finalI).getIsLecSign() != Status.BLANK && !signETUSES.get(finalI).getLecText().trim().equals("")) {
                            showPopup(v, signETUSES.get(finalI).getLecText(), false);
                        }
                    });

                    // sem
                    ImageView semView = currentChildLayout.findViewById(R.id.txtEtSem);
                    switch (signETUSES.get(finalI).getIsSemSign()) {
                        case PASSED:
                            semView.setImageResource(R.drawable.ic_check_24dp);
                            break;
                        case FAILED:
                            semView.setImageResource(R.drawable.ic_close_24dp);
                            break;
                        case BLANK:
                            semView.setImageResource(0);
                            break;
                    }
                    semView.setOnClickListener(v -> {
                        if (signETUSES.get(finalI).getIsSemSign() != Status.BLANK && !signETUSES.get(finalI).getSemText().trim().equals("")) {
                            showPopup(v, signETUSES.get(finalI).getSemText(), false);
                        }
                    });

                    // lab
                    ImageView labView = currentChildLayout.findViewById(R.id.txtEtLab);
                    switch (signETUSES.get(finalI).getIsLabSign()) {
                        case PASSED:
                            labView.setImageResource(R.drawable.ic_check_24dp);
                            break;
                        case FAILED:
                            labView.setImageResource(R.drawable.ic_close_24dp);
                            break;
                        case BLANK:
                            labView.setImageResource(0);
                            break;
                    }
                    labView.setOnClickListener(v -> {
                        if (signETUSES.get(finalI).getIsLabSign() != Status.BLANK && !signETUSES.get(finalI).getLabText().trim().equals("")) {
                            showPopup(v, signETUSES.get(finalI).getLabText(), false);
                        }
                    });

                    // prac
                    ImageView pracView = currentChildLayout.findViewById(R.id.txtEtPrac);
                    switch (signETUSES.get(finalI).getIsPracSign()) {
                        case PASSED:
                            pracView.setImageResource(R.drawable.ic_check_24dp);
                            break;
                        case FAILED:
                            pracView.setImageResource(R.drawable.ic_close_24dp);
                            break;
                        case BLANK:
                            pracView.setImageResource(0);
                            break;
                    }
                    pracView.setOnClickListener(v -> {
                        if (signETUSES.get(finalI).getIsPracSign() != Status.BLANK && !signETUSES.get(finalI).getPracText().trim().equals("")) {
                            showPopup(v, signETUSES.get(finalI).getPracText(), false);
                        }
                    });
                    holder.childItems.addView(rootView);
                }
            } else {
                holder.imageView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return semesters == null ? 0 : semesters.size();
    }

    private void showPopup(View clickedView, String text, boolean isRight) {
        final View popupView = LayoutInflater.from(mContext).inflate(R.layout.popup_sign, null);

        TextView textView = popupView.findViewById(R.id.explanation_sign);
        textView.setText(text);

        final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        popupWindow.setAnimationStyle(R.style.popup_window_animation);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setTouchable(true);
        popupWindow.setBackgroundDrawable(new ColorDrawable());

        Rect rect = locateView(clickedView);
        if (rect != null) {
            popupWindow.showAtLocation(popupView, Gravity.NO_GRAVITY, isRight ? rect.right : rect.left, rect.bottom);
        }

    }

    private Rect locateView(View v) {
        int[] loc_int = new int[2];
        if (v == null) return null;
        try {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe) {
            //Happens when the view doesn't exist on screen anymore.
            return null;
        }
        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1];
        location.right = location.left + v.getWidth();
        location.bottom = location.top + v.getHeight();
        return location;
    }

    public class SignEtSemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mTitle;
        private LinearLayout childItems;
        private ImageView imageView;

        public SignEtSemViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mTitle = itemView.findViewById(R.id.title);
            imageView = itemView.findViewById(R.id.icon);
            childItems = itemView.findViewById(R.id.childItems);
            childItems.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View v) {
            if (childItems.getVisibility() == View.VISIBLE) {
                imageView.setImageResource(R.drawable.ic_keyboard_arrow_right_black_24dp);
                childItems.setVisibility(View.GONE);
            } else {
                if (childItems.getChildCount() > 0) {
                    for (int i = 0; i < recyclerView.getChildCount(); i++) {
                        SignEtSemViewHolder holder1 = (SignEtSemViewHolder) recyclerView.getChildViewHolder(recyclerView.getChildAt(i));
                        if (i != getLayoutPosition()) {
                            holder1.imageView.setImageResource(R.drawable.ic_keyboard_arrow_right_black_24dp);
                            holder1.childItems.setVisibility(View.GONE);
                        }
                    }
                }
                imageView.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                childItems.setVisibility(View.VISIBLE);
            }
        }
    }
}