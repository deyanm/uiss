package com.deyanm.uiss.data;

public enum Status {
    PASSED, FAILED, BLANK
}
