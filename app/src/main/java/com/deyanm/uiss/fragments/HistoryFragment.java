package com.deyanm.uiss.fragments;

import static com.deyanm.uiss.retrofit.APIClient.BASE_URL;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.deyanm.uiss.R;
import com.deyanm.uiss.retrofit.APIClient;
import com.deyanm.uiss.retrofit.APIService;
import com.deyanm.uiss.utils.ActivityUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryFragment extends Fragment {

    private static final String TAG = HistoryFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private APIService apiService;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        apiService = APIClient.getClient(view.getContext(), false).create(APIService.class);
        apiService.getSecurity(CookieManager.getInstance().getCookie(BASE_URL)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    List<History> histories = new ArrayList<>();
                    Document document = Jsoup.parse(response.body().string());
                    Elements trElements = document.select("table.list_table").select("tr[align=center]");
                    trElements.size();
                    for (Element trEl : trElements) {
                        Elements tdEl = trEl.select("td");
                        if (tdEl.size() == 4 && trEl != trElements.get(0)) {
                            histories.add(new History(tdEl.get(0).text(), tdEl.get(2).text(), tdEl.get(3).text()));
                        }
                    }
                    setDataAdapter(histories);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                ActivityUtils.showServerError(getActivity(), t);
                call.cancel();
            }
        });

        recyclerView = view.findViewById(R.id.historyRecycler);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    public void setDataAdapter(List<History> markList) {
        recyclerView.setAdapter(new HistoryAdapter(markList));
    }

    public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.HistoryViewHolder> {

        private List<History> historyList;

        public HistoryAdapter(List<History> historyList) {
            this.historyList = historyList;
        }

        @NonNull
        @Override
        public HistoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            return new HistoryViewHolder(inflater.inflate(R.layout.item_history, parent, false));
        }

        @Override
        public void onBindViewHolder(HistoryViewHolder holder, int position) {
            holder.mDate.setText(historyList.get(position).getDate());
            holder.mInfo.setText(historyList.get(position).getInfo());
            holder.mConn.setText(historyList.get(position).getConn());

        }

        @Override
        public int getItemCount() {
            return historyList == null ? 0 : historyList.size();
        }

        public class HistoryViewHolder extends RecyclerView.ViewHolder {

            private TextView mDate, mInfo, mConn;

            public HistoryViewHolder(View itemView) {
                super(itemView);

                mDate = itemView.findViewById(R.id.historyDate);
                mInfo = itemView.findViewById(R.id.historyInfo);
                mConn = itemView.findViewById(R.id.historyConnect);
            }
        }
    }

    public class History {
        private String date;
        private String info;
        private String conn;

        public History(String date, String info, String conn) {
            this.date = date;
            this.info = info;
            this.conn = conn;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getInfo() {
            return info;
        }

        public void setInfo(String info) {
            this.info = info;
        }

        public String getConn() {
            return conn;
        }

        public void setConn(String conn) {
            this.conn = conn;
        }
    }

}
