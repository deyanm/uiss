package com.deyanm.uiss.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.deyanm.uiss.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class InfoFragment extends Fragment {
    private static final String html = "html";

    private String htmlData;
    private RecyclerView recyclerView;
    private InfoAdapter dessertAdapter;

    public InfoFragment() {
        // Required empty public constructor
    }

    public static InfoFragment newInstance(String htmlData) {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        args.putString(html, htmlData);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            htmlData = getArguments().getString(html);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.scrollableview);
        //  Use when your list size is constant for better performance
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

        Document document = Jsoup.parse(htmlData);
        Elements trElements = document.select("table[cellspacing=3]").get(0).select("tr");
        List<String> firstEl = new ArrayList<>();
        for (Element element : trElements.select("td[align=right][valign=top]")) {
            firstEl.add(element.text());
        }
        List<String> secElement = new ArrayList<>();
        for(Element element : trElements.select("td[align=left][valign=top]")) {
            secElement.add(element.text());
        }

        dessertAdapter = new InfoAdapter(firstEl, secElement);
        recyclerView.setAdapter(dessertAdapter);

    }

    public class InfoAdapter extends RecyclerView.Adapter<InfoAdapter.InfoViewHolder> {

        private List<String> firstEl;
        private List<String> secEl;

        public InfoAdapter(List<String> firstEl, List<String> secEl) {
            this.firstEl = firstEl;
            this.secEl = secEl;
        }

        @NonNull
        @Override
        public InfoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.item_info, parent, false);
            return new InfoAdapter.InfoViewHolder(view);
        }

        @Override
        public void onBindViewHolder(InfoViewHolder holder, int position) {
            holder.mFirstEl.setText(firstEl.get(position));
            holder.mSecEl.setText(secEl.get(position));

        }

        @Override
        public int getItemCount() {
            return firstEl == null ? 0 : firstEl.size();
        }

        public class InfoViewHolder extends RecyclerView.ViewHolder {

            private TextView mFirstEl;
            private TextView mSecEl;

            public InfoViewHolder(View itemView) {
                super(itemView);

                mFirstEl = itemView.findViewById(R.id.txtFirst);
                mSecEl = itemView.findViewById(R.id.txtSec);
            }
        }
    }

}
