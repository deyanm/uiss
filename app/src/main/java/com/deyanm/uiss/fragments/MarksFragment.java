package com.deyanm.uiss.fragments;

import static com.deyanm.uiss.retrofit.APIClient.BASE_URL;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.deyanm.uiss.R;
import com.deyanm.uiss.data.Mark;
import com.deyanm.uiss.data.MarksAdapter;
import com.deyanm.uiss.data.Semester;
import com.deyanm.uiss.retrofit.APIClient;
import com.deyanm.uiss.retrofit.APIService;
import com.deyanm.uiss.utils.ActivityUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MarksFragment extends Fragment {

    private static final String TAG = MarksFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private APIService apiService;

    public MarksFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_marks, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        apiService = APIClient.getClient(view.getContext(), false).create(APIService.class);
        apiService.getMarks(CookieManager.getInstance().getCookie(BASE_URL)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    int i = -1;
                    List<Semester> semesters = new ArrayList<>();
                    Document document = Jsoup.parse(response.body().string());
                    Elements trElements = document.select("table.list_table").select("tr[align=center]");

                    for (Element trEl : trElements) {
                        Elements tdEl = trEl.select("td");

                        if (tdEl.size() == 1 && tdEl.get(0).hasClass("subtitle")) {
                            Semester semester = new Semester();
                            semester.setTitle(tdEl.get(0).html());
                            semesters.add(semester);
                            i++;
                        }
                        if (tdEl.size() == 5 && trEl != trElements.get(0)) {
                            List<Mark> marks;
                            if (semesters.get(i).getList() != null) {
                                marks = (List<Mark>) semesters.get(i).getList();
                            } else {
                                marks = new ArrayList<>();
                                semesters.get(i).setList(marks);
                            }
                            marks.add(new Mark(tdEl.get(0).text(), tdEl.get(1).text(), tdEl.get(2).text(), tdEl.get(3).text(), tdEl.get(4).text()));
                        }
                    }
                    setDataAdapter(semesters);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                ActivityUtils.showServerError(getActivity(), t);
                call.cancel();
            }
        });

        recyclerView = view.findViewById(R.id.marksRecycler);
        //  Use when your list size is constant for better performance
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    public void setDataAdapter(List<Semester> markList) {
        recyclerView.setAdapter(new MarksAdapter(getContext(), markList));

        // automatically open the last semester from the view
        recyclerView.postDelayed(() -> recyclerView.scrollToPosition(markList.size() - 1),300);
        recyclerView.postDelayed((Runnable) () -> recyclerView.findViewHolderForAdapterPosition(markList.size() - 2).itemView.performClick(),400);
    }

}
