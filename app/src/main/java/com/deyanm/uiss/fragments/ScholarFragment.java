package com.deyanm.uiss.fragments;

import static com.deyanm.uiss.retrofit.APIClient.BASE_URL;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.deyanm.uiss.R;
import com.deyanm.uiss.retrofit.APIClient;
import com.deyanm.uiss.retrofit.APIService;
import com.deyanm.uiss.utils.ActivityUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScholarFragment extends Fragment {

    private static final String TAG = ScholarFragment.class.getSimpleName();
    private RecyclerView firstRecyclerView, secondRecyclerView;
    private APIService apiService;

    public ScholarFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_health, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        apiService = APIClient.getClient(view.getContext(), false).create(APIService.class);
        apiService.getHealth(CookieManager.getInstance().getCookie(BASE_URL)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    List<FirstHealth> firstHealthList = new ArrayList<>();
                    List<SecondHealth> secondHealthList = new ArrayList<>();
                    Document document = Jsoup.parse(response.body().string());
                    ((TextView) view.findViewById(R.id.headerHealth)).setText(document.select("td[colspan=2]").select("span").text());
                    Elements tables = document.select("table.list_table");
                    Elements trElements1 = tables.get(0).select("tr[align=center]");
                    trElements1.size();
                    for (Element trEl : trElements1) {
                        Elements tdEl = trEl.select("td");
                        if (tdEl.size() == 4 && trEl != trElements1.get(0)) {
                            firstHealthList.add(new FirstHealth(tdEl.get(0).text(), tdEl.get(1).text(), tdEl.get(2).text(), tdEl.get(3).text()));
                        }
                    }

                    Elements trElements2 = tables.get(1).select("tr[align=center]");
                    trElements2.size();
                    for (Element trEl : trElements2) {
                        Elements tdEl = trEl.select("td");
                        if (tdEl.size() == 3 && trEl != trElements2.get(0)) {
                            secondHealthList.add(new SecondHealth(tdEl.get(0).text(), tdEl.get(1).text(), tdEl.get(2).text()));
                        }
                    }
                    setDataAdapter(firstHealthList, secondHealthList);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                ActivityUtils.showServerError(getActivity(), t);
                call.cancel();
            }
        });

        firstRecyclerView = view.findViewById(R.id.healthFirstRecycler);
        secondRecyclerView = view.findViewById(R.id.healthSecondRecycler);
        firstRecyclerView.setHasFixedSize(true);
        secondRecyclerView.setHasFixedSize(true);
        firstRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        secondRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    public void setDataAdapter(List<FirstHealth> firstHealthList, List<SecondHealth> secondHealths) {
        firstRecyclerView.setAdapter(new FirstAdapter(firstHealthList));
        secondRecyclerView.setAdapter(new SecondAdapter(secondHealths));
    }

    public class FirstAdapter extends RecyclerView.Adapter<FirstAdapter.FirstViewHolder> {

        private List<FirstHealth> firstHealthList;

        public FirstAdapter(List<FirstHealth> firstHealthList) {
            this.firstHealthList = firstHealthList;
        }

        @NonNull
        @Override
        public FirstViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            return new FirstViewHolder(inflater.inflate(R.layout.item_first_health, parent, false));
        }

        @Override
        public void onBindViewHolder(FirstViewHolder holder, int position) {
            holder.mYear.setText(firstHealthList.get(position).getYear());
            holder.mMonth.setText(firstHealthList.get(position).getMonth());
            holder.mSum.setText(firstHealthList.get(position).getSum());
            holder.mOsig.setText(firstHealthList.get(position).getOsig());

        }

        @Override
        public int getItemCount() {
            return firstHealthList == null ? 0 : firstHealthList.size();
        }

        public class FirstViewHolder extends RecyclerView.ViewHolder {

            private TextView mYear, mMonth, mSum, mOsig;

            public FirstViewHolder(View itemView) {
                super(itemView);

                mYear = itemView.findViewById(R.id.firstYear);
                mMonth = itemView.findViewById(R.id.firstMonth);
                mSum = itemView.findViewById(R.id.firstSum);
                mOsig = itemView.findViewById(R.id.firstOsig);
            }
        }
    }

    public class SecondAdapter extends RecyclerView.Adapter<SecondAdapter.SecondViewHolder> {

        private List<SecondHealth> secondHealths;

        public SecondAdapter(List<SecondHealth> firstHealthList) {
            this.secondHealths = firstHealthList;
        }

        @NonNull
        @Override
        public SecondViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            return new SecondViewHolder(inflater.inflate(R.layout.item_second_health, parent, false));
        }

        @Override
        public void onBindViewHolder(SecondViewHolder holder, int position) {
            holder.mDate.setText(secondHealths.get(position).getDate());
            holder.mFrom.setText(secondHealths.get(position).getFromDate());
            holder.mType.setText(secondHealths.get(position).getType());

        }

        @Override
        public int getItemCount() {
            return secondHealths == null ? 0 : secondHealths.size();
        }

        public class SecondViewHolder extends RecyclerView.ViewHolder {

            private TextView mDate, mFrom, mType;

            public SecondViewHolder(View itemView) {
                super(itemView);

                mDate = itemView.findViewById(R.id.secondDate);
                mFrom = itemView.findViewById(R.id.secondFromDate);
                mType = itemView.findViewById(R.id.secondType);
            }
        }
    }

    public class FirstHealth {
        private String year;
        private String month;
        private String sum;
        private String osig;

        public FirstHealth(String year, String month, String sum, String osig) {
            this.year = year;
            this.month = month;
            this.sum = sum;
            this.osig = osig;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getSum() {
            return sum;
        }

        public void setSum(String sum) {
            this.sum = sum;
        }

        public String getOsig() {
            return osig;
        }

        public void setOsig(String osig) {
            this.osig = osig;
        }
    }

    public class SecondHealth {
        private String date;
        private String fromDate;
        private String type;

        public SecondHealth(String date, String fromDate, String type) {
            this.date = date;
            this.fromDate = fromDate;
            this.type = type;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getFromDate() {
            return fromDate;
        }

        public void setFromDate(String fromDate) {
            this.fromDate = fromDate;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

}
