package com.deyanm.uiss.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.deyanm.uiss.R;

public class SettingsFragment extends Fragment {

    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getActivity() != null) {
            mPreferences = getActivity().getSharedPreferences("UISS-APP", Context.MODE_PRIVATE);
            mEditor = mPreferences.edit();
        }

        SwitchCompat rememberMe = view.findViewById(R.id.rememberMe);
        if (!mPreferences.contains("fak") && !mPreferences.contains("egn")) {
            rememberMe.setEnabled(false);
        } else {
            rememberMe.setChecked(mPreferences.getBoolean("remember", false));
        }
        rememberMe.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (!isChecked) {
                mEditor.remove("fak");
                mEditor.remove("egn");
            }
            mEditor.putBoolean("remember", isChecked).apply();
        });

        TextView textView = view.findViewById(R.id.privacyLink);
        textView.setClickable(true);
        textView.setMovementMethod(LinkMovementMethod.getInstance());
        textView.setText(Html.fromHtml("<a href='https://deyanm.github.io/static/myuiss/privacy_policy.html'>Декларация за поверителност и общи условия</a>"));

    }

}
