package com.deyanm.uiss.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.deyanm.uiss.R;
import com.deyanm.uiss.data.Semester;
import com.deyanm.uiss.data.SignETUS;
import com.deyanm.uiss.data.SignEtAdapter;
import com.deyanm.uiss.data.Status;
import com.deyanm.uiss.retrofit.APIClient;
import com.deyanm.uiss.retrofit.APIService;
import com.deyanm.uiss.utils.ActivityUtils;
import com.deyanm.uiss.utils.EncryptionServices;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignETUSFragment extends Fragment {

    private static final String TAG = SignETUSFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private APIService apiService;
    private String fak, egn;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private Context mContext;

    public SignETUSFragment() {
        // Required empty public constructor
    }

    public static SignETUSFragment newInstance(Bundle b) {
        SignETUSFragment fragment = new SignETUSFragment();
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            fak = getArguments().getString("fak");
            egn = getArguments().getString("egn");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_etus, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mPreferences = view.getContext().getSharedPreferences("UISS-APP", Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();

        apiService = APIClient.getClient(view.getContext(), true).create(APIService.class);
        apiService.loginETUS(
                mPreferences.contains("fak") ? new EncryptionServices(view.getContext()).decrypt(mPreferences.getString("fak", "3f394a452787428c14492fefece18b33")) : fak,
                mPreferences.contains("egn") ? new EncryptionServices(view.getContext()).decrypt(mPreferences.getString("egn", "3f394a452787428c14492fefece18b33")) : egn).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                loginToServer();
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                ActivityUtils.showServerError(getActivity(), t);
                call.cancel();
            }
        });

        recyclerView = view.findViewById(R.id.etusRecycler);
        //  Use when your list size is constant for better performance
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("fak", fak);
        outState.putString("egn", egn);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore last state for checked position.
            fak = savedInstanceState.getString("fak", "");
            egn = savedInstanceState.getString("egn", "");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    public void loginToServer() {
        apiService.loginETUS(
                mPreferences.contains("fak") ? new EncryptionServices(mContext).decrypt(mPreferences.getString("fak", "3f394a452787428c14492fefece18b33")) : fak,
                mPreferences.contains("egn") ? new EncryptionServices(mContext).decrypt(mPreferences.getString("egn", "3f394a452787428c14492fefece18b33")) : egn).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    int i = -1;
                    String html = null;
                    if (response.body() != null) {
                        html = response.body().string();
                    }
                    List<Semester> semesters = new ArrayList<>();
                    Document document = Jsoup.parse(html);
                    Elements trElements = document.select("table[cellspacing=0]").select("tr");
                    for (Element trEl : trElements) {
                        Elements tdEl = trEl.select("td");

                        if (!trEl.select("th[colspan=5]").text().isEmpty()) {
                            Semester semester = new Semester();
                            semester.setTitle(trEl.select("th[colspan=5]").text());
                            semesters.add(semester);
                            i++;
                        }
                        if (tdEl.size() == 5 && trEl != trElements.get(0)) {
                            List<SignETUS> signETUSES;
                            if (semesters.get(i).getList() != null) {
                                signETUSES = (List<SignETUS>) semesters.get(i).getList();
                            } else {
                                signETUSES = new ArrayList<>();
                                semesters.get(i).setList(signETUSES);
                            }
                            SignETUS signETUS = new SignETUS();
                            signETUS.setDisc(tdEl.get(0).text());

                            // lections
                            String imgAttr = tdEl.get(1).select("img").attr("src");
                            signETUS.setIsLecSign(imgAttr.equals("passed.gif") ? Status.PASSED : imgAttr.equals("failed.gif") ? Status.FAILED : Status.BLANK);
                            signETUS.setLecText(tdEl.get(1).select("center").text());

                            // lab upr
                            String imgAttr1 = tdEl.get(2).select("img").attr("src");
                            signETUS.setIsLabSign(imgAttr1.equals("passed.gif") ? Status.PASSED : imgAttr1.equals("failed.gif") ? Status.FAILED : Status.BLANK);
                            signETUS.setLabText(tdEl.get(2).select("center").text());

                            // sem upr
                            String imgAtt2 = tdEl.get(3).select("img").attr("src");
                            signETUS.setIsSemSign(imgAtt2.equals("passed.gif") ? Status.PASSED : imgAtt2.equals("failed.gif") ? Status.FAILED : Status.BLANK);
                            signETUS.setSemText(tdEl.get(3).select("center").text());

                            // practise
                            String imgAttr3 = tdEl.get(4).select("img").attr("src");
                            signETUS.setIsPracSign(imgAttr3.equals("passed.gif") ? Status.PASSED : imgAttr3.equals("failed.gif") ? Status.FAILED : Status.BLANK);
                            signETUS.setPracText(tdEl.get(4).select("center").text());
                            signETUSES.add(signETUS);
                        }
                    }
                    setDataAdapter(semesters);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, Throwable t) {
                String cause;
                if (t instanceof IOException) {
                    cause = "Проблем със свързването, проверете дали имате активна интернет връзка!";
                } else {
                    cause = String.valueOf(t.getMessage());
                }
                ActivityUtils.showSnackBar(getActivity().findViewById(android.R.id.content), cause);
                call.cancel();
            }
        });
    }

    public void setDataAdapter(List<Semester> semesters) {
        recyclerView.setAdapter(new SignEtAdapter(mContext, semesters));
    }
}
