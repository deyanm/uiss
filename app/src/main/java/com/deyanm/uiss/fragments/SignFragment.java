package com.deyanm.uiss.fragments;

import static com.deyanm.uiss.retrofit.APIClient.BASE_URL;

import android.content.Context;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.deyanm.uiss.R;
import com.deyanm.uiss.data.Semester;
import com.deyanm.uiss.data.Sign;
import com.deyanm.uiss.data.Status;
import com.deyanm.uiss.retrofit.APIClient;
import com.deyanm.uiss.retrofit.APIService;
import com.deyanm.uiss.utils.ActivityUtils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignFragment extends Fragment {

    private static final String TAG = SignFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private APIService apiService;
    private Context mContext;
    private PopupWindow popupWindow;

    public SignFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = view.getContext();
        apiService = APIClient.getClient(view.getContext(), false).create(APIService.class);
        apiService.getCerts(CookieManager.getInstance().getCookie(BASE_URL)).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                try {
                    int i = -1;
                    List<Semester> semesters = new ArrayList<>();
                    Document document = Jsoup.parse(response.body().string());
                    Elements trElements = document.select("table.list_table").select("tr[align=center]");

                    for (Element trEl : trElements) {
                        Elements tdEl = trEl.select("td");

                        if (tdEl.size() == 1 && tdEl.get(0).hasClass("subtitle")) {
                            Semester semester = new Semester();
                            semester.setTitle(tdEl.get(0).html());
                            semesters.add(semester);
                            i++;
                        }
                        if (tdEl.size() == 7 && trEl != trElements.get(0)) {
                            List<Sign> marks;
                            if (semesters.get(i).getList() != null) {
                                marks = (List<Sign>) semesters.get(i).getList();
                            } else {
                                marks = new ArrayList<>();
                                semesters.get(i).setList(marks);
                            }
                            Sign sign = new Sign();
                            sign.setId(tdEl.get(0).text());
                            sign.setDisc(tdEl.get(1).text());
                            sign.setLect(tdEl.get(2).text());
                            sign.setIsLectSign(tdEl.get(2).text().length() > 5 ? Status.PASSED : tdEl.get(2).text().equals("Не") ? Status.FAILED : Status.BLANK);

                            sign.setSem(tdEl.get(3).text());
                            sign.setIsSemSign(tdEl.get(3).text().length() > 5 ? Status.PASSED : tdEl.get(3).text().equals("Не") ? Status.FAILED : Status.BLANK);

                            sign.setLab(tdEl.get(4).text());
                            sign.setIsLabSign(tdEl.get(4).text().length() > 5 ? Status.PASSED : tdEl.get(4).text().equals("Не") ? Status.FAILED : Status.BLANK);

                            sign.setPract(tdEl.get(5).text());
                            sign.setIsPractSign(tdEl.get(5).text().length() > 5 ? Status.PASSED : tdEl.get(5).text().equals("Не") ? Status.FAILED : Status.BLANK);

                            sign.setProj(tdEl.get(6).text());
                            sign.setIsProjSign(tdEl.get(6).text().length() > 5 ? Status.PASSED : tdEl.get(6).text().equals("Не") ? Status.FAILED : Status.BLANK);

                            marks.add(sign);
//                            marks.add(new Sign(tdEl.get(0).text(), tdEl.get(1).text(), tdEl.get(2).text(), tdEl.get(3).text(), tdEl.get(4).text(), tdEl.get(5).text(), tdEl.get(6).text()));
                        }
                    }
                    setDataAdapter(semesters);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                ActivityUtils.showServerError(getActivity(), t);
                call.cancel();
            }
        });

        recyclerView = view.findViewById(R.id.signRecycler);
        //  Use when your list size is constant for better performance
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);

    }

    public void setDataAdapter(List<Semester> semesters) {
        recyclerView.setAdapter(new SignAdapter(mContext, semesters));

        // automatically open the last semester from the view
        recyclerView.postDelayed(() -> recyclerView.scrollToPosition(semesters.size() - 1),300);
        recyclerView.postDelayed((Runnable) () -> recyclerView.findViewHolderForAdapterPosition(semesters.size() - 1).itemView.performClick(),400);
    }

    public class SignAdapter extends RecyclerView.Adapter<SignAdapter.SignSemViewHolder> {

        private List<Semester> semesters;
        private Context mContext;
        private RecyclerView recyclerView;

        public SignAdapter(Context context, List<Semester> semesters) {
            mContext = context;
            this.semesters = semesters;
        }

        @Override
        public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
            super.onAttachedToRecyclerView(recyclerView);
            this.recyclerView = recyclerView;
        }


        @NonNull
        @Override
        public SignSemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new SignSemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sign_vh, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull SignSemViewHolder holder, final int position) {
            Semester semester = semesters.get(position);

            if (semester != null) {
                if (semester.getTitle() != null) {
                    holder.mTitle.setText(Html.fromHtml(semester.getTitle()));
                }
                if (semester.getList() != null && semester.getList().size() != 0) {
                    int childSize = semester.getList().size();
                    holder.childItems.removeAllViews();
                    View tableHeader = View.inflate(mContext, R.layout.sem_header_signs, null);
                    holder.childItems.addView(tableHeader);
                    for (int i = 0; i < childSize; i++) {
                        View rootView = View.inflate(mContext, R.layout.item_sign, null);
                        LinearLayout currentChildLayout = rootView.findViewById(R.id.signsRoot);
                        List<Sign> signList = (List<Sign>) semester.getList();
                        ((TextView) currentChildLayout.findViewById(R.id.txtCount)).setText(signList.get(i).getId());
                        ((TextView) currentChildLayout.findViewById(R.id.txtDisc)).setText(signList.get(i).getDisc());
//                        ((TextView) currentChildLayout.findViewById(R.id.txtSignLect)).setText(marks.get(i).getLect());
//                        ((TextView) currentChildLayout.findViewById(R.id.txtSignSem)).setText(marks.get(i).getSem());
//                        ((TextView) currentChildLayout.findViewById(R.id.txtSignLab)).setText(marks.get(i).getLab());
//                        ((TextView) currentChildLayout.findViewById(R.id.txtSignPract)).setText(marks.get(i).getPract());
//                        ((TextView) currentChildLayout.findViewById(R.id.txtSignProj)).setText(marks.get(i).getProj());

                        int finalI = i;
                        // lec
                        ImageView lecView = currentChildLayout.findViewById(R.id.txtSignLect);
                        switch (signList.get(finalI).getIsLectSign()) {
                            case PASSED:
                                lecView.setImageResource(R.drawable.ic_check_24dp);
                                break;
                            case FAILED:
                                lecView.setImageResource(R.drawable.ic_close_24dp);
                                break;
                            case BLANK:
                                lecView.setImageResource(android.R.color.transparent);
                                break;
                        }
                        lecView.setOnClickListener(v -> {
                            if (signList.get(finalI).getIsLectSign() != Status.BLANK && !signList.get(finalI).getLect().trim().equals("")) {
                                showPopup(v, signList.get(finalI).getLect(), false);
//                                if (tooltip != null) {
//                                    tooltip.dismiss();
//                                }
//                                tooltip = new Tooltip.Builder(mContext, lecView).setText(signList.get(finalI).getLect()).show();
                            }
                        });

                        // sem
                        ImageView semView = currentChildLayout.findViewById(R.id.txtSignSem);
                        switch (signList.get(finalI).getIsSemSign()) {
                            case PASSED:
                                semView.setImageResource(R.drawable.ic_check_24dp);
                                break;
                            case FAILED:
                                semView.setImageResource(R.drawable.ic_close_24dp);
                                break;
                            case BLANK:
                                semView.setImageResource(android.R.color.transparent);
                                break;
                        }
                        semView.setOnClickListener(v -> {
                            if (signList.get(finalI).getIsSemSign() != Status.BLANK && !signList.get(finalI).getSem().trim().equals("")) {
                                showPopup(v, signList.get(finalI).getSem(), false);
//                                if (tooltip != null) {
//                                    tooltip.dismiss();
//                                }
//                                tooltip = new Tooltip.Builder(mContext, semView).setText(signList.get(finalI).getSem()).show();
                            }
                        });

                        // lab
                        ImageView labView = currentChildLayout.findViewById(R.id.txtSignLab);
                        switch (signList.get(finalI).getIsLabSign()) {
                            case PASSED:
                                labView.setImageResource(R.drawable.ic_check_24dp);
                                break;
                            case FAILED:
                                labView.setImageResource(R.drawable.ic_close_24dp);
                                break;
                            case BLANK:
                                labView.setImageResource(android.R.color.transparent);
                                break;
                        }
                        labView.setOnClickListener(v -> {
                            if (signList.get(finalI).getIsLabSign() != Status.BLANK && !signList.get(finalI).getLab().trim().equals("")) {
                                showPopup(v, signList.get(finalI).getLab(), false);
//                                if (tooltip != null) {
//                                    tooltip.dismiss();
//                                }
//                                tooltip = new Tooltip.Builder(mContext, labView).setText(signList.get(finalI).getLab()).show();
                            }
                        });

                        // prac
                        ImageView pracView = currentChildLayout.findViewById(R.id.txtSignPract);
                        switch (signList.get(finalI).getIsPractSign()) {
                            case PASSED:
                                pracView.setImageResource(R.drawable.ic_check_24dp);
                                break;
                            case FAILED:
                                pracView.setImageResource(R.drawable.ic_close_24dp);
                                break;
                            case BLANK:
                                pracView.setImageResource(android.R.color.transparent);
                                break;
                        }
                        pracView.setOnClickListener(v -> {
                            if (signList.get(finalI).getIsPractSign() != Status.BLANK && !signList.get(finalI).getPract().trim().equals("")) {
                                showPopup(v, signList.get(finalI).getPract(), false);
//                                if (tooltip != null) {
//                                    tooltip.dismiss();
//                                }
//                                tooltip = new Tooltip.Builder(mContext, pracView).setText(signList.get(finalI).getPract()).show();
                            }
                        });

                        // proj
                        ImageView projView = currentChildLayout.findViewById(R.id.txtSignProj);
                        switch (signList.get(finalI).getIsProjSign()) {
                            case PASSED:
                                projView.setImageResource(R.drawable.ic_check_24dp);
                                break;
                            case FAILED:
                                projView.setImageResource(R.drawable.ic_close_24dp);
                                break;
                            case BLANK:
                                projView.setImageResource(android.R.color.transparent);
                                break;
                        }
                        projView.setOnClickListener(v -> {
                            if (signList.get(finalI).getIsProjSign() != Status.BLANK && !signList.get(finalI).getProj().trim().equals("")) {
                                showPopup(v, signList.get(finalI).getProj(), false);
//                                if (tooltip != null) {
//                                    tooltip.dismiss();
//                                }
//                                tooltip = new Tooltip.Builder(mContext, projView).setText(signList.get(finalI).getProj()).show();
                            }
                        });

                        holder.childItems.addView(rootView);
                    }
                } else {
                    holder.imageView.setVisibility(View.GONE);
                }
            }
        }

        @Override
        public int getItemCount() {
            return semesters == null ? 0 : semesters.size();
        }

        private void showPopup(View clickedView, String text, boolean isRight) {
            final View popupView = LayoutInflater.from(mContext).inflate(R.layout.popup_sign, null);

            TextView textView = popupView.findViewById(R.id.explanation_sign);
            textView.setText(text);

//            final PopupWindow popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
//            popupWindow.setAnimationStyle(R.style.popup_window_animation);
//            popupWindow.setOutsideTouchable(true);
//            popupWindow.setTouchable(true);
//            popupWindow.setBackgroundDrawable(new ColorDrawable());
//
//            Rect rect = locateView(clickedView);
//            if (rect != null) {
//                popupWindow.showAtLocation(popupView, Gravity.NO_GRAVITY, isRight ? rect.right : rect.left, rect.bottom);
//            }
            if (popupWindow != null) {
                popupWindow.dismiss();
            }

            popupWindow = new PopupWindow(popupView, WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            popupWindow.setAnimationStyle(R.style.popup_window_animation);
            popupWindow.setOutsideTouchable(true);
            popupWindow.setTouchable(true);
            popupWindow.setFocusable(true);
            popupWindow.setBackgroundDrawable(new ColorDrawable(getResources().getColor(android.R.color.transparent)));

            Rect rect = locateView(clickedView);
            if (rect != null) {
                popupWindow.showAtLocation(popupView, Gravity.NO_GRAVITY, isRight ? rect.right : rect.left, rect.bottom);
            }

        }

        private Rect locateView(View v) {
            int[] loc_int = new int[2];
            if (v == null) return null;
            try {
                v.getLocationOnScreen(loc_int);
            } catch (NullPointerException npe) {
                //Happens when the view doesn't exist on screen anymore.
                return null;
            }
            Rect location = new Rect();
            location.left = loc_int[0];
            location.top = loc_int[1];
            location.right = location.left + v.getWidth();
            location.bottom = location.top + v.getHeight();
            return location;
        }

        public class SignSemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private TextView mTitle;
            private LinearLayout childItems;
            private ImageView imageView;

            public SignSemViewHolder(View itemView) {
                super(itemView);
                itemView.setOnClickListener(this);
                mTitle = itemView.findViewById(R.id.title);
                imageView = itemView.findViewById(R.id.icon);
                childItems = itemView.findViewById(R.id.childItems);
                childItems.setVisibility(View.GONE);
            }

            @Override
            public void onClick(View v) {
                if (childItems.getVisibility() == View.VISIBLE) {
                    imageView.setImageResource(R.drawable.ic_keyboard_arrow_right_black_24dp);
                    childItems.setVisibility(View.GONE);
                } else {
                    if (childItems.getChildCount() > 0) {
                        for (int i = 0; i < recyclerView.getChildCount(); i++) {
                            SignSemViewHolder holder1 = (SignSemViewHolder) recyclerView.getChildViewHolder(recyclerView.getChildAt(i));
                            if (i != getLayoutPosition()) {
                                holder1.imageView.setImageResource(R.drawable.ic_keyboard_arrow_right_black_24dp);
                                holder1.childItems.setVisibility(View.GONE);
                            }
                        }
                    }
                    imageView.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                    childItems.setVisibility(View.VISIBLE);
                }
            }
        }
    }
}
