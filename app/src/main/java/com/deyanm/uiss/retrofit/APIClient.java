package com.deyanm.uiss.retrofit;

import android.app.Application;
import android.content.Context;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.deyanm.uiss.utils.ActivityUtils;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.cache.CacheInterceptor;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {

    private static Retrofit retrofit = null;

    public static final String BASE_URL = "https://student.tu-sofia.bg/";

    private static final Long CACHE_SIZE = 5 * 1024 * 1024L;

    public static final String CACHE_CONTROL_HEADER = "Cache-Control";
    public static final String CACHE_CONTROL_NO_CACHE = "no-cache";

    public Cache httpCache(Application application) {
        return new Cache(application.getApplicationContext().getCacheDir(), CACHE_SIZE);
    }

    public static Retrofit getClient(Context context, boolean isEtus) {

        try {

            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.level(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                    .readTimeout(60, TimeUnit.SECONDS)
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
//                    .addInterceptor(new ReceivedCookiesInterceptor(context))
                    .addInterceptor(new AddCookiesInterceptor(context))
                    .hostnameVerifier((hostname, session) -> true);

            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
                SSLContext sslcontext = SSLContext.getInstance("TLSv1");
                sslcontext.init(null, trustAllCerts, new java.security.SecureRandom());

                SSLSocketFactory NoSSLv3Factory = new NoSSLv3SocketFactory(sslcontext.getSocketFactory());
                clientBuilder.sslSocketFactory(NoSSLv3Factory, (X509TrustManager) trustAllCerts[0]);
            } else {
                // Install the all-trusting trust manager
                final SSLContext sslContext = SSLContext.getInstance("SSL");
                sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

                // Create an ssl socket factory with our all-trusting manager
                final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
                clientBuilder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            }

            OkHttpClient client = clientBuilder.build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(isEtus ? "https://e-university.tu-sofia.bg/ETUS/" : BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();

            return retrofit;
        } catch (Exception e) {
            e.printStackTrace();
            AlertDialog.Builder messageBox = new AlertDialog.Builder(context);
            messageBox.setTitle("Проблем при свързването със сървъра");
            messageBox.setMessage("Пробвайте да рестартирате приложението, ако проблемът продължава, се свържете с мен!");
            messageBox.setCancelable(false);
            messageBox.setNeutralButton("ОК", null);
            messageBox.show();
        }
        return retrofit;
    }
//
//    static class CacheInterceptor implements Interceptor {
//
//        @NonNull
//        @Override
//        public Response intercept(@NonNull Chain chain) throws IOException {
//            Request request = chain.request();
//            Response originalResponse = chain.proceed(request);
//
//            boolean shouldUseCache = !request.header(CACHE_CONTROL_HEADER).equals(CACHE_CONTROL_NO_CACHE);
//
//            if (!shouldUseCache) return originalResponse;
//
//            CacheControl cacheControl = new CacheControl.Builder()
//                    .maxAge(10, TimeUnit.MINUTES)
//                    .build();
//
//            return originalResponse.newBuilder()
//                    .header(CACHE_CONTROL_HEADER, cacheControl.toString())
//                    .build();
//        }
//    }

}
