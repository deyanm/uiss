package com.deyanm.uiss.retrofit;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {

    /*
     * Methods for UISS data
     * */
    @FormUrlEncoded
    @POST("index.php")
    Call<ResponseBody> login(@Field("egn") String egn, @Field("fn") String fn);

    @GET("info.php?logout=true")
    Call<ResponseBody> logout();

    @GET("info.php")
    Call<ResponseBody> getStudentInfo(@Header("Cookie") String cookie);

    @GET("health.php")
    Call<ResponseBody> getHealth(@Header("Cookie") String cookie);

    @GET("marks.php")
    Call<ResponseBody> getMarks(@Header("Cookie") String cookie);

    @GET("certs.php")
    Call<ResponseBody> getCerts(@Header("Cookie") String cookie);

    @GET("security.php")
    Call<ResponseBody> getSecurity(@Header("Cookie") String cookie);

    @Headers("Accept: image/avif,image/webp,image/apng,image/svg+xml,image/*,*/*;q=0.8")
    @GET("photo.php")
    Call<ResponseBody> getStudentPhoto(@Header("Cookie") String cookie);

    /*
     * Methods for ETUS data
     * */
    @FormUrlEncoded
    @POST("zaverki/index.php")
    Call<ResponseBody> loginETUS(@Field("egn") String egn, @Field("fnum") String fn);
}
