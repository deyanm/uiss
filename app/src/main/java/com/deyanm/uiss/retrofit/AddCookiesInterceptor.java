package com.deyanm.uiss.retrofit;

import android.content.Context;
import android.os.Build;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class AddCookiesInterceptor implements Interceptor {
    public static final String PREF_COOKIES = "PREF_COOKIES";

    private Context context;

    public AddCookiesInterceptor(Context context) {
        this.context = context;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request.Builder builder = chain.request().newBuilder();

        String cookie = context.getSharedPreferences("UISS-APP", Context.MODE_PRIVATE).getString(PREF_COOKIES, "");

        builder.header("User-Agent", "myUISS App (Android SDK Version: " + Build.VERSION.SDK_INT + ", Device: " + Build.MANUFACTURER + ", Model: " + Build.MODEL + ")");
        builder.addHeader("Cookie", cookie);

        return chain.proceed(builder.build());
    }
}