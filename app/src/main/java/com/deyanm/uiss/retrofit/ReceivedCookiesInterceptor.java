package com.deyanm.uiss.retrofit;

import android.content.Context;
import android.content.SharedPreferences;
import androidx.annotation.NonNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ReceivedCookiesInterceptor implements Interceptor {
    private Context context;

    public ReceivedCookiesInterceptor(Context context) {
        this.context = context;
    } // AddCookiesInterceptor()

    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        Request request = chain.request();
        Response originalResponse = chain.proceed(chain.request());

        SharedPreferences preferences = context.getSharedPreferences("UISS-APP", Context.MODE_PRIVATE);
        if (!preferences.contains("PREF_COOKIES")) {
            SharedPreferences.Editor editor = preferences.edit();
            if (!request.headers("Cookie").isEmpty()) {
                String cookie = request.header("Cookie");

                editor.putString("PREF_COOKIES", cookie).apply();
            } else if (!originalResponse.headers("Set-Cookie").isEmpty()) {
                String cookie = originalResponse.header("Set-Cookie");

                editor.putString("PREF_COOKIES", cookie).apply();
            }
        }

        return originalResponse;
    }
}
