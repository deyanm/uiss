package com.deyanm.uiss.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.View;
import android.widget.TextView;

import androidx.core.content.ContextCompat;

import com.deyanm.uiss.R;
import com.google.android.material.snackbar.Snackbar;

import java.io.IOException;

public class ActivityUtils {

    public static void showSnackBar(View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        View sbView = snackbar.getView();
        TextView textView = sbView
                .findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(ContextCompat.getColor(view.getContext(), R.color.colorWhite));
        snackbar.show();
    }

    public static void showServerError(Activity activity, Throwable t) {
        String cause;
        if (t instanceof IOException) {
            cause = "Проблем със свързването, проверете дали имате активна интернет връзка!";
        } else {
            cause = String.valueOf(t.getMessage());
        }
        showSnackBar(activity.findViewById(android.R.id.content), cause);
    }

    public static boolean hasNetwork(Context context) {
        boolean isConnected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected())
            isConnected = true;
        return isConnected;
    }

    public static int dpToPx(int dp)
    {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px)
    {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }
}
