package com.deyanm.uiss.utils

import android.content.Context

class EncryptionServices(context: Context) {

    companion object {
        val MASTER_KEY = "MASTER_KEY"
    }

    private val keyStoreWrapper = KeyStoreWrapper(context)

    /*
     * Encryption Stage
     */

    fun doesAndroidMasterKeyExist():Boolean {
        return keyStoreWrapper.doesAndroidMasterKeyExist(MASTER_KEY)
    }

    fun createMasterKey(keyPassword: String? = null) {
        keyStoreWrapper.createAndroidKeyStoreAsymmetricKey(MASTER_KEY)
    }

    fun removeMasterKey() {
        keyStoreWrapper.removeAndroidKeyStoreKey(MASTER_KEY)
    }

    fun encrypt(data: String): String {
        val masterKey = keyStoreWrapper.getAndroidKeyStoreAsymmetricKeyPair(MASTER_KEY)
        return CipherWrapper(CipherWrapper.TRANSFORMATION_ASYMMETRIC).encrypt(data, masterKey?.public)
    }

    fun decrypt(data: String): String? {
        val masterKey = keyStoreWrapper.getAndroidKeyStoreAsymmetricKeyPair(MASTER_KEY)
        return CipherWrapper(CipherWrapper.TRANSFORMATION_ASYMMETRIC).decrypt(data, masterKey?.private)
    }
}